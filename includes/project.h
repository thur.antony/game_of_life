/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   project.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/09 23:02:48 by avykhova          #+#    #+#             */
/*   Updated: 2018/05/09 23:02:54 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef PROJECT_H
# define PROJECT_H

# include "project.h"
# include "get_next_line.h"
//# include <mlx.h>
# include "../libmlx/mlx.h"
# include <math.h>
# include <fcntl.h>

# define WIN_WIDTH 768
# define WIN_HEIGHT 768
# define IMG_LEN WIN_WIDTH * WIN_HEIGHT  * 4
# define STEP 12
# define CELLS 4096
# define ROW 64

# define YELLOW_LIGHT 0xFFFF66
# define YELLOW 0xFFFF00
# define WHITE_YELLOW 0xFFFFE0
# define DARK_CYAN 0x1996A0
# define CYAN 0x00FFFF
# define WHITE 0xFFFFFF
# define BLACK 0
# define MAGENDA_DARK 0x8B008B
# define MAGENDA 0xFF00FF
# define PALE_VIOLET_RED 0xDB7093
# define LEMON_CHIFFON 0xFFFACD
# define SLATE_GRAY 0x708890

# define OFFSET 1
# define RECT_SIZE STEP - OFFSET

# define ALLOC_CHECK(x) if (!x) return ;

/*
**	HELPER STRUCTS
**	----------------------------------------------------------------
*/

/*
**	MAIN STRUCTS
**	----------------------------------------------------------------
*/
typedef struct		s_data
{
	char			*name;
	void			*mlx_ptr;
	void			*win_ptr;
	void			*img_ptr;
	char			*image;
	int				bpp;
	int				size_line;
	int				end;

	int				x;
	int				y;
	char			click;

	char			**cells;
	int				nextgen;
	char			mode;
	int				ruleset;

}					t_data;

/*
**	FUNCTIONALITY
**	----------------------------------------------------------------
*/
t_data				init(char *name);

int					keyboard(int key, t_data *dt);
int					mouse(int button, int x, int y, t_data *dt);
int					cell_automata(t_data *dt);
void				apply_ruleset(t_data *dt);

/*
** PRIMITIVES
**	----------------------------------------------------------------
*/
void				point(t_data *dt, int x, int y, int color);
void				rect(t_data *dt, int x, int y, int color, int size);
void				vert_line(t_data *dt, int x, int color);
void				hor_line(t_data *dt, int y, int color);
void				grid(t_data *dt, int step, int color);
void				backgound_rect(t_data *dt, int color);
void				generation_put(t_data *dt, int color);
void				frame(t_data *dt);
/*
**	HELPERS
**	-----------------------------------------------------------------
*/


#endif
