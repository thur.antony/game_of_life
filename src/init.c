/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   init.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/09 23:00:54 by avykhova          #+#    #+#             */
/*   Updated: 2018/05/09 23:00:59 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "project.h"

t_data		init(char *name)
{
	t_data	dt;

	dt.name = name;
	dt.mlx_ptr = mlx_init();
	dt.win_ptr = mlx_new_window(dt.mlx_ptr, WIN_WIDTH, WIN_HEIGHT, name);
	dt.img_ptr = mlx_new_image(dt.mlx_ptr, WIN_WIDTH, WIN_HEIGHT + 1);
	dt.image = mlx_get_data_addr(dt.img_ptr, &dt.bpp, &dt.size_line, &dt.end);
	dt.bpp /= 8;

	dt.cells = (char **)malloc(sizeof(char *) * 2);
	dt.cells[0] = ft_strnew(CELLS);
	dt.cells[1] = ft_strnew(CELLS);

	dt.mode = 0;
	dt.click = 0;
	dt.nextgen = 0;
	/*
	** ruleset == 0: Conway's Game of Life
	*/
	dt.ruleset = 0;

	return (dt);
}
