/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rulesets.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/09 23:01:21 by avykhova          #+#    #+#             */
/*   Updated: 2018/05/09 23:01:24 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "project.h"

static void		game_of_life(t_data *dt)
{
	int			i;
	int			end_i;
	int			counter;

	i = ROW;
	end_i = CELLS - ROW;
	counter = 0;
	while (++i < end_i)
	{
		if (dt->cells[!dt->nextgen][i - ROW - 1])
			counter++;
		if (dt->cells[!dt->nextgen][i - ROW])
			counter++;
		if (dt->cells[!dt->nextgen][i - ROW + 1])
			counter++;
		if (dt->cells[!dt->nextgen][i - 1])
			counter++;
		if (dt->cells[!dt->nextgen][i + 1])
			counter++;
		if (dt->cells[!dt->nextgen][i + ROW - 1])
			counter++;
		if (dt->cells[!dt->nextgen][i + ROW])
			counter++;
		if (dt->cells[!dt->nextgen][i + ROW + 1])
			counter++;
		// if (counter < 2 || counter > 3)
		// 	dt->cells[dt->nextgen][i] = 0;
		// if (counter == 2 || counter == 3)
		// 	dt->cells[dt->nextgen][i] = 1;
		if (dt->cells[!dt->nextgen][i] == 1)
		{
			if (counter < 2 || counter > 3)
				dt->cells[dt->nextgen][i] = 0;
			if (counter == 2 || counter == 3)
				dt->cells[dt->nextgen][i] = 1;
		}
		if (dt->cells[!dt->nextgen][i] == 0)
		{
			if (counter == 3)
				dt->cells[dt->nextgen][i] = 1;
		}
		counter = 0;
	}
}

void			apply_ruleset(t_data *dt)
{
	if (dt->ruleset == 0)
		game_of_life(dt);
}
