/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/09 23:01:10 by avykhova          #+#    #+#             */
/*   Updated: 2018/05/09 23:01:14 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "project.h"

/*
** WHITE_YELLOW + DARK_CYAN - school copybook
** BLACK + SLATE_GRAY
** 
*/

void		frame(t_data *dt)
{
	backgound_rect(dt, BLACK);
	//grid(dt, STEP, SLATE_GRAY);

	generation_put(dt, YELLOW_LIGHT);

	mlx_put_image_to_window(dt->mlx_ptr, dt->win_ptr, dt->img_ptr, 0, 0);
	ft_bzero(dt->image, IMG_LEN);
}

int			main(__attribute__((unused))int argc, char **argv)
{
	__attribute__((unused))t_data	dt;

	dt = init(argv[1]);
	frame(&dt);
	mlx_loop_hook(dt.mlx_ptr, cell_automata, &dt);
	mlx_mouse_hook(dt.win_ptr, mouse, &dt);
	mlx_key_hook(dt.win_ptr, keyboard, &dt);
	mlx_loop(dt.mlx_ptr);
	return (0);
}
