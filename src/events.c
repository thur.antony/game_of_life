/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   events.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/09 23:00:45 by avykhova          #+#    #+#             */
/*   Updated: 2018/05/09 23:00:49 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "project.h"

int			keyboard(int key, t_data *dt)
{
	if (key == 49)
	{
		dt->mode = !dt->mode;
		dt->nextgen = 1;
	}
	if (dt->mode == 0)
	{
		ft_bzero(dt->cells[0], CELLS);
		ft_bzero(dt->cells[1], CELLS);
		dt->nextgen = 0;
	}
	return (1);
}

int			mouse(__attribute__((unused))int button, int x, int y, t_data *dt)
{
	if (dt->mode == 0)
	{
		dt->x = x;
		dt->y = y;
		dt->click = 1;
	}
	return (1);
}


/*
** helper mechanics -- generation_put
*/
void			generation_put(t_data *dt, int color)
{
	int			i;
	int			x;
	int			y;

	i = -1;
	while (++i < CELLS)
	{
		if (dt->cells[dt->nextgen][i] != 0)
		{
			y = (i / ROW) * STEP;
			x = (i % ROW) * STEP;
			rect(dt, x + OFFSET, y + OFFSET, RECT_SIZE, color);
		}
	}
}

/*
**	dt->mode == 0: starting conditions.
**	dt->mode == 1: run scenario.
*/
int			cell_automata(t_data *dt)
{
	int		index;

	if (!dt->mode)
	{
		if (dt->click)
		{
			index = (dt->x / STEP) + (ROW * (dt->y / STEP));
			dt->cells[0][index] = !dt->cells[0][index];
			dt->click = 0;
		}
		frame(dt);
	}
	else
	{
		/*
		** flipping mechanics
		*/
		apply_ruleset(dt);
		frame(dt);
		ft_bzero(dt->cells[!dt->nextgen], CELLS);
		dt->nextgen = !dt->nextgen;
	}
	return (1);
}
