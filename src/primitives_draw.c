/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   primitives_draw.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/05/09 23:01:32 by avykhova          #+#    #+#             */
/*   Updated: 2018/05/09 23:01:35 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "project.h"

void			point(t_data *dt, int x, int y, int color)
{
	int			byte;

	if (x > WIN_WIDTH || x < 0 || y > WIN_HEIGHT || y < 0)
		return ;
	byte = (x * dt->bpp) + (y * dt->size_line);
	dt->image[byte] = color;
	dt->image[++byte] = color >> 8;
	dt->image[++byte] = color >> 16;
}

void			rect(t_data *dt, int x, int y, int size, int color)
{
	int			x_lim;
	int			y_lim;
	int			x_0;

	x_lim = x + size;
	y_lim = y + size;
	x_0 = x;
	while (y < y_lim)
	{
		while (x < x_lim)
		{
			point(dt, x, y, color);
			x++;
		}
		x = x_0;
		y++;
	}
}

void			backgound_rect(t_data *dt, int color)
{
	rect(dt, 0, 0, WIN_WIDTH, color);
}

void			vert_line(t_data *dt, int x, int color)
{
	int			y;

	y = -1;
	while (++y < WIN_HEIGHT)
		point(dt, x, y, color);
}

void			hor_line(t_data *dt, int y, int color)
{
	int			x;

	x = -1;
	while (++x < WIN_WIDTH)
		point(dt, x, y, color);
}

void			grid(t_data *dt, int step, int color)
{
	int			x;
	int			y;

	y = 0 + step;
	x = 0 + step;
	while (y <= WIN_HEIGHT)
	{
		hor_line(dt, y, color);
		y += step;
	}
	while (x <= WIN_WIDTH)
	{
		vert_line(dt, x, color);
		x += step;
	}
}
