#include <stdio.h>

int		main(void)
{
	int	x;
	int	res;

	x = 0;
	while (x < 20)
	{
		res = x * 512;
		printf("size == %d, x == %d\n", res, x);
		x++;
	}
	return (0);
}