/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: avykhova <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/18 13:16:15 by avykhova          #+#    #+#             */
/*   Updated: 2017/12/18 16:13:07 by avykhova         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#define DUMB 922337203685477580

int		ft_atoi(const char *s)
{
	size_t				i;
	int					sign;
	unsigned long int	res;

	i = 0;
	sign = 1;
	res = 0;
	while (ft_isspace(s[i]))
		i++;
	if (s[i] == '-' || s[i] == '+')
	{
		if (s[i] == '-')
			sign = -sign;
		i++;
	}
	while (ft_isdigit(s[i]))
	{
		if ((res > DUMB || (res == DUMB && (s[i] - 48) > 7)) && sign == 1)
			return (-1);
		else if ((res > DUMB || (res == DUMB && (s[i] - 48) > 8)) && sign == -1)
			return (0);
		res = res * 10 + s[i++] - 48;
	}
	return ((int)(res * sign));
}
